#!/usr/bin/env bash

# Script to push image to ecs repo
export AWS_PROFILE=ecs

eval "$(aws ecr get-login --no-include-email --region us-east-1)"

docker build -t summary .

docker tag summary:latest 792525955519.dkr.ecr.us-east-1.amazonaws.com/qa:latest

docker push 792525955519.dkr.ecr.us-east-1.amazonaws.com/qa:latest