#!/usr/bin/env bash

docker build -t summary .

if docker run -i -t --name summary -p 8000:80 summary ; then
    echo "success"
else
    docker stop summary
    docker rm summary
    docker run -i -t --name summary -p 8000:80 summary
fi
