FROM basesummary

# Port to expose
EXPOSE 80

# Copy project code
WORKDIR /code
#COPY . $DOCK_SRVHOME

ENV DEBUG="false"

COPY ./django_nginx.conf /etc/nginx/sites-available/
RUN ln -s /etc/nginx/sites-available/django_nginx.conf /etc/nginx/sites-enabled
RUN echo "daemon off;" >> /etc/nginx/nginx.conf


RUN pip3 install django-cors-headers social-auth-app-django psycopg2-binary

ADD . /code/

RUN python manage.py collectstatic --clear --noinput # clearstatic files
RUN python manage.py collectstatic --noinput --noinput  # collect static files
