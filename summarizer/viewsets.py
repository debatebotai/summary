from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.permissions import IsAuthenticated, AllowAny

from summarizer.summary import summary
from cloudsearch.search.articles import article_search_cloud
from cloudsearch.search.search import handle_article_response
from cloudsearch.search.academic import SearchElasticSearch

from services.kg.kg import ConnectNeo4j
from services.kg.search import get_papers
from debatebot.exceptions import create_error


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def lb_test(request):
    return Response({"status": "load test lb"}, status=200)


@api_view((['GET']))
def qa_landing(request):
    return Response({}, template_name='qa-landing.html')


@api_view((['GET']))
def summary_landing(request):
    return Response({}, template_name='summary-landing.html')


@api_view(["POST"])
# @permission_classes((IsAuthenticated, ))
@renderer_classes((JSONRenderer, TemplateHTMLRenderer))
def article_summaries_cloud(request):
    text = request.data.get('text')
    params = {"articles": '', "errors": {}}
    if text:
        summaries = []
        resp, err = article_search_cloud(text)
        if err:
            return Response(err, status=400)

        articles, err = handle_article_response(resp)
        if err:
            return Response(err, status=400)

        if not articles:
            err = {"error": "no article matches"}
            return Response(err, status=400)
        print(articles)
        for u, art in articles:
                data = summary(art)
                summaries.append(data)

        if len(summaries) == 0:
            params['errors']['summary_error'] = "error with summaries."
        params['articles'] = summaries
        return Response(params, template_name='summary-result.html')
    else:
        params['errors']['text_error'] = "No text found"
        return Response(params, template_name='summary-result.html')


@api_view(["POST"])
# @permission_classes((IsAuthenticated, ))
@permission_classes((AllowAny, ))
@renderer_classes((JSONRenderer, TemplateHTMLRenderer))
def academic_summaries_elastic(request):
    print("authentication")
    text = request.data.get('text')
    params = {"articles": '', "errors": {}}
    if text:
        summaries = []
        es = SearchElasticSearch()
        resp, err = es.search(text, "body")
        if err:
            return Response(err, status=400)
        articles, err = es.handle_response(resp)
        if err:
            return Response(err, status=400)

        if not articles:
            err = {"error": "no article matches"}
            return Response(err, status=400)

        for art in articles:
                data = summary(art)
                summaries.append(data)

        if len(summaries) == 0:
            params['errors']['summary_error'] = "error with summaries."
        params['articles'] = summaries[:3]
        return Response(params, template_name='summary-result.html')
    else:
        params['errors']['text_error'] = "No text found"
        return Response(params, template_name='summary-result.html', status=400)


@api_view(["POST"])
@renderer_classes((JSONRenderer, TemplateHTMLRenderer))
def upload_paper_url(request):
    """
    1. Accepts url
    2. Calls get paper function
    3. Write to db using kg app
    :return:
    """
    url = request.data.get('url')
    if url:
        user_paper, rel_papers, error = get_papers(url)
        if error:
            return Response(error, status=400)

        neo = ConnectNeo4j()
        with neo.driver.session() as session:
            q = neo.create_merge_statements(rel_papers)
            try:
                session.write_transaction(neo.add_user_papers, q, user_paper)
            except Exception as e:
                return Response(create_error(e.__str__()), status=400)
                # return Response(create_error("failed to write to kg"), status=400)

            try:
                # List of paper text
                user_paper_title = user_paper.get('title')
                papers = session.read_transaction(neo.get_top_papers, user_paper_title)
                papers.append(user_paper.get('text'))
                all_text = ' '.join(papers[:5])
                total_summary = summary(all_text)
            except:
                return Response(create_error("error finding papers"), status=400)

        return Response({"summaries": total_summary}, status=200)
