from django.test import Client
from django.urls import reverse
from django.test import TestCase


class TestViewSets(TestCase):

    # def __init__(self, *args, **kwargs):
    #     super().__init__()

    def test_upload_paper(self):
        resp = self.client.post(reverse("user_upload_paper"), {"url": "google"})
        print(resp)
        self.assertEqual(resp.status_code, 200)