from django.apps import AppConfig
from django.core.cache import cache


class SummarizerConfig(AppConfig):
    name = 'summarizer'

    def ready(self):
        pass
        # try:
        #     f = open('QA/Data/w2v_dictionary', 'rb')
        #     cache.set('w2v_300', cpickle.load(f), None)
        #     f.close()
        #     print('### already pickled ####')
        # except:
        #     print('##### not pickled ####')
        #     CACHE_MODEL_NAME_KEY = 'model_name'
        #     CACHE_MODEL_NAME = 'rnet'

        # print("loading vectors")
        # w2v_300 = KeyedVectors.load('QA/Data/gensim/gensim-300.bin', mmap='r')
        # w2v_300.syn0norm = w2v_300.syn0  # prevent recalc of normed vectors
        # w2v_300.most_similar('stuff')  # any word will do: just to page all in
        # Semaphore(0).acquire()  # just hang until process killed

        # if cache.get('w2v_300') is None:
            # CACHE_MODEL_NAME_KEY = 'model_name'
            # CACHE_MODEL_NAME = 'rnet'
            #
            # CACHE_DEBUG_KEY = "debug"
            # CACHE_DEBUG = False
            #
            # CACHE_DATASET_KEY = 'dataset'
            # CACHE_DATASET = 'dev'
            #
            # CACHE_MODEL_PATH_KEY = 'model_path'
            # CACHE_MODEL_PATH = 'QA/Models/Save/rnet_model27.ckpt'

            # modOpts = json.load(open('Models/config.json', 'r'))[args.model]['dev']
            # CACHE_MOD_OPTS = json.load(open('QA/Models/config.json', 'r'))['rnet']['dev']
            # cache.set('mod_opts', CACHE_MOD_OPTS)


            # cache_model = model_rnet.R_NET(CACHE_MOD_OPTS)

            # CACHE_INPUT_TENSORS, CACHE_LOSS, CACHE_ACC, CACHE_PRED_SI, CACHE_PRED_EI = \
            #     cache_model.build_model()
            # Save input tensors, loss, accuracy, predictions to cache

            # input_tensors, loss, acc, pred_si, pred_ei = CACHE_MODEL_NAME.build_model()
            # saved_model = args.model_path
            # saved_model = CACHE_MODEL_PATH

            # config = tf.ConfigProto()
            # config.gpu_options.allow_growth = True
            # new_saver = tf.train.Saver()
            # sess = tf.InteractiveSession(config=config)
            # new_saver.restore(sess, saved_model)
            # pred_data = {}

            # EM = 0.0
            # F1 = 0.0
            # empty_answer_idx = np.ndarray((CACHE_MOD_OPTS['batch_size'], CACHE_MOD_OPTS['p_length']))
            # cache.get_or_set('empty_answer_idx', empty_answer_idx)

            # Original w2v_300 to restore accuracy
            # w2v_300 = {}
            # with open('QA/Data/glove.840B.300d.txt', 'r', encoding='utf-8') as fh:
            #     for line in fh:
            #         array = line.lstrip().rstrip().split(" ")
            #         word = array[0]
            #         vector = list(map(float, array[1:]))
            #         w2v_300[word] = vector

            # Best w2v_300 supposedly.
            # w2v_300 = {}
            # with open('QA/Data/glove.840B.300d.txt', 'r', encoding='utf-8') as fh:
            #     for line in fh:
            #         array = line.lstrip().rstrip().split(" ")
            #         word = array[0]
            #         vector = list(map(float, array[1:]))
            #         w2v_300[word] = vector
            #
            # for key in w2v_300:
            #     word = key
            #     vector = w2v_300[key]
            #     if word in w2v_300:
            #         w2v_300[word] = vector
            #     if word.capitalize() in w2v_300:
            #         w2v_300[word.capitalize()] = vector
            #     if word.lower() in w2v_300:
            #         w2v_300[word.lower()] = vector
            #     if word.upper() in w2v_300:
            #         w2v_300[word.upper()] = vector


        #
        #     # cache.set('sess', sess, None)
        #     with open('/home/ubuntu/Desktop/debatebot/QA/Data/w2v_dictionary', 'wb') as f:
        #         cpickle.dump(w2v_300, f)
        #     print("##### pickled #####")
        # # else:
        # #     cache.set('w2v_300', cpickle.load('/home/ubuntu/Desktop/debatebot/QA/Data/w2v_dictionary'))
        # #     print('done, already pickled!')
