# from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers

from summarizer.viewsets import article_summaries_cloud, lb_test, academic_summaries_elastic, upload_paper_url
# router = routers.SimpleRouter()
# router.register('^$', SearchViewSet, base_name="search")

urlpatterns = [
    path("search/", article_summaries_cloud, name='summary-cloud'),
    path("test", lb_test, name='test'),
    path("academic/search/", academic_summaries_elastic, name="academic-summary-elastic"),
    path("academic/upload", upload_paper_url, name="user_upload_paper")
]
