
"""
Created on Sat Jan  6 15:40:39 2018

@author: fanat and clomil
"""


import nltk
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer


stemmer = PorterStemmer()
lemmatizer = WordNetLemmatizer()
stop_words = set(stopwords.words('english'))


def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed


def tokenize_stem(text):
    tokens = nltk.word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems


def tokenize_sent(text):
    sent_tokens = nltk.sent_tokenize(text)
    return sent_tokens


def summary(text):
    """Compares the similarity of a sentence vs the entire document in order to
        find most relevant sentences. Best 5 sentences are taken."""

    tfidf = TfidfVectorizer(tokenizer=tokenize_stem, stop_words=stop_words)
    tfs = tfidf.fit_transform([text])
    pos = []
    sentences = tokenize_sent(text)
    for count, i in enumerate(sentences):
        pos.append([cosine_similarity(tfidf.transform([i]), tfs), count])
    place = sorted(pos, reverse=True)[:8]
    summary = [sentences[i] for i in sorted([i[1] for i in place])]
    summary = ' '.join(summary)
    return summary
