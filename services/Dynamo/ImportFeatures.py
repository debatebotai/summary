# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 20:37:06 2018

@author: fanat
"""

from __future__ import print_function # Python 2/3 compatibility
import boto3
import json
import datetime
import pandas
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from sklearn.metrics.pairwise import cosine_similarity 

dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:8000")
#dynamodb = boto3.resource('dynamodb', region_name='us-east-1', endpoint_url="http://dynamodb.us-east-1.amazonaws.com")
stemmer = PorterStemmer()
lemmatizer = WordNetLemmatizer()




def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

def lem_tokens(tokens, lemmatizer):
    lemmed = []
    for item in tokens:
        lemmed.append(lemmatizer.lemmatize(item))
    return lemmed

def tokenize_stem(text):
    tokens = nltk.word_tokenize(text)
    stems = stem_tokens(tokens, stemmer)
    return stems

def tokenize_lem(text):
    tokens = nltk.word_tokenize(text)
    lems = lem_tokens(tokens, lemmatizer)
    return lems

def tokenize_sent(text):
    sent_tokens = nltk.sent_tokenize(text)
    return sent_tokens

def createEntry(name, timeStamp, text):
    table = dynamodb.Table('CompanyDocuments')
    table.put_item(
        Item={
            'Time': timeStamp,
            'Name': name,
            'Text': text
            }
        )

def lower_remove_punctuation(text):
    lowered = text.lower()
    no_punctuation = lowered.translate(str.maketrans('','',string.punctuation))
    return no_punctuation

#def get_tfidf(text):
#    tfidf = TfidfVectorizer(tokenizer=tokenize_stem, stop_words='english')
#    tfs = tfidf.fit_transform({text})
#    return tfs.toarray()

def addFeatures(name, timeStamp, text):
    table = dynamodb.Table('CompanyDocuments')
    response = table.update_item(
        Key={
            'Time': timeStamp,
            'Name': name
        },
        UpdateExpression="set rawText = :r, Stemming = :s, Lemmatizing = :l, Sentence = :sent, Cleaned = :c",
        ExpressionAttributeValues={
            ':r': text,
            ':s': tokenize_stem(text),
            ':l': tokenize_lem(text),
            ':sent': tokenize_sent(text),
            ':c' : lower_remove_punctuation(text)

        },
        ReturnValues="UPDATED_NEW"
    )
    
    
#def compareSimilarity():
#    cosine_similarity(get_tfidf(cleaned), get_tfidf(temptext))
#    


corpus = pandas.read_csv("articles1.csv")
for index, each_article in corpus.iterrows():
    timeStamp = time.ctime()
    addFeatures(each_article['title'], timeStamp, each_article['content'])




try:
    response = table.get_item(
        Key={
            'Time': timeStamp,
            'Name': name
        }
    )
except ClientError as e:
    print(e.response['Error']['Message'])
else:
    item = response['Item']
    print("GetItem succeeded:")
    print(json.dumps(item, indent=4, cls=DecimalEncoder))
    
    
    
