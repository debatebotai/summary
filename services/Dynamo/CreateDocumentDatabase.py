# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 22:51:59 2018

@author: fanat
"""


from __future__ import print_function # Python 2/3 compatibility

import os
os.environ["TZ"] = "UTC"


import boto3
#dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:8000")
dynamodb = boto3.resource('dynamodb', region_name='us-east-1', endpoint_url="http://dynamodb.us-east-1.amazonaws.com")



table = dynamodb.create_table(
    TableName='test',
    KeySchema=[
        {
            'AttributeName': 'Time',
            'KeyType': 'HASH'  #Partition key
        },
        {
            'AttributeName': 'Name',
            'KeyType': 'RANGE'  #Sort key
        }
    ],
    AttributeDefinitions=[
        {
            'AttributeName': 'Time',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'Name',
            'AttributeType': 'S'
        },

    ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }
)

print("Table status:", table.table_status)

