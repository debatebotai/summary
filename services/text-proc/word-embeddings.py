from gensim.scripts.glove2word2vec import glove2word2vec
from gensim.models import KeyedVectors
import sys


# Takes word2vec text and converts to bin file
def create_word2vec_bin(file):
    print('converting glove vectors')
    glove2word2vec(file, 'word2vec-output.txt')
    print('creating word2vec vectors')
    model = KeyedVectors.load_word2vec_format('word2vec-output.txt')
    model.init_sims(replace=True)
    print('saving vectors')
    model.save('QA/Data/gensim/gensim-300.bin')


create_word2vec_bin('QA/Data/glove.840B.300d.txt')
