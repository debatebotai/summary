from django.apps import AppConfig
from django.core.cache import cache

import pandas as pd

from gensim.corpora import Dictionary
from gensim.models import TfidfModel
from gensim.similarities.docsim import MatrixSimilarity
from gensim.similarities.docsim import Similarity


local_articles_dir = 'ArticleMatch/Data/articles/articles1.csv'
local_dir = 'ArticleMatch/Data'
dev_dir = '/home/ubuntu/Desktop/qa/ArticleMatch/Data'
articles_dev_dir = '/home/ubuntu/Downloads/articles1.csv'


def documents_load():
    data = pd.read_csv(local_articles_dir)
    documents = list(data['content'])
    print("### CLEANING ###")
    token_docs = clean(documents[:200])
    print("### SAVING DICTIONARY")
    dictionary = Dictionary(token_docs)
    dictionary.save(local_dir + '/dictionary')
    print("### TFIDF")
    corpus = [dictionary.doc2bow(doc) for doc in token_docs]
    tf_idf = TfidfModel(corpus)
    tf_idf.save(local_dir + '/tfidf')
    print("### SIMILARITY")
    # matrix_sim = MatrixSimilarity(tf_idf[corpus], num_features=len(dictionary), num_best=5)
    sims = Similarity('ArticleMatch/Data/', tf_idf[corpus], num_features=len(dictionary), num_best=5)
    sims.save(local_dir + '/docs.index')

''' Cleans each document - removing stop words, commas, and lowercases each word
'''


def clean(documents):
    stoplist = set('for a of the and to in'.split())
    texts = [[word for word in document.lower().split() if word not in stoplist]
             for document in documents]

    # remove words that appear only once
    from collections import defaultdict
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1

    texts = [[token for token in text if frequency[token] > 1]
             for text in texts]

    from pprint import pprint  # pretty-printer
    # pprint(texts[5])
    return texts


def ready():
    print("#### Loading Docs ####")
    documents_load()


ready()