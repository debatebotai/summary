from django.apps import AppConfig
from django.core.cache import cache

import pandas as pd

from gensim.corpora import Dictionary
from gensim.models import TfidfModel
from gensim.similarities.docsim import MatrixSimilarity
from gensim.similarities.docsim import Similarity


local_dir = 'ArticleMatch/Data'
dev_dir = '/home/ubuntu/Desktop/qa/ArticleMatch/Data'
articles_dev_dir = '/home/ubuntu/Downloads/articles1.csv'


class VectorsConfig(AppConfig):
    name = 'ArticleMatch'

    def load_vectors(self):
        pass
    #     dictionary = Dictionary().load(local_dir + '/dictionary')
    #     print('#### dictionary loaded', str(dictionary))
    #     tf_idf = TfidfModel().load(local_dir + '/tfidf')
    #     print('#### tf_idf loaded')
    #     sims = Similarity.load(local_dir + '/docs.index')
    #     print('#### similarity loaded')
    #     # corpus = pd.read_csv(articles_dev_dir)
    #
    #     cache.set('dictionary', dictionary, None)
    #     cache.set('tf_idf', tf_idf, None)
    #     cache.set('sims', sims, None)
    #     # cache.set('corpus', corpus)
    #
    #
    # ''' Loads corpus into memory, processed and in original form
    # '''
    #
    # # def documents_load(self):
    # #     data = pd.read_csv(dev_dir)
    # #     documents = list(data['content'])
    # #     token_docs = self.clean(documents[:10000])
    # #
    # #     token_docs = cache.get('token_docs')
    # #     dictionary = Dictionary(token_docs)
    # #     corpus = [dictionary.doc2bow(doc) for doc in token_docs]
    # #     tf_idf = TfidfModel(corpus)
    # #     # matrix_sim = MatrixSimilarity(tf_idf[corpus], num_features=len(dictionary), num_best=5)
    # #     sims = Similarity('/ArticleMatch/Data/', tf_idf[corpus], num_features=len(dictionary), num_best=5)
    # #
    # #     cache.set('similarity', sims)
    # #     cache.set('tf_idf', tf_idf)
    # #     cache.set('dictionary', Dictionary(token_docs))
    # #
    # #     # cache.set('corpus', corpus)
    # #     # cache.get_or_set('token_docs', token_docs)
    # #     # cache.set('documents', documents)
    # #     # return token_docs, documents
    # #
    # # ''' Cleans each document - removing stop words, commas, and lowercases each word
    # # '''
    # # @staticmethod
    # # def clean(self, documents):
    # #     stoplist = set('for a of the and to in'.split())
    # #     texts = [[word for word in document.lower().split() if word not in stoplist]
    # #              for document in documents]
    # #
    # #     # remove words that appear only once
    # #     from collections import defaultdict
    # #     frequency = defaultdict(int)
    # #     for text in texts:
    # #         for token in text:
    # #             frequency[token] += 1
    # #
    # #     texts = [[token for token in text if frequency[token] > 1]
    # #              for text in texts]
    # #
    # #     from pprint import pprint  # pretty-printer
    # #     # pprint(texts[5])
    # #     return texts

    def ready(self):
        # self.load_vectors()
        pass


