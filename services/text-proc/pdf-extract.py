import PyPDF2
import sys
print(sys.path)
# creating a pdf file object
pdfFileObj = open('research.pdf', 'rb')

# creating a pdf reader object
pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

# printing number of pages in pdf file
print(pdfReader.numPages)

# creating a page object
pageObj = pdfReader.getPage(0)

# extracting text from page
test = open("output.txt", 'w')
# print(pageObj.extractText())
raw_text = pageObj.extractText()
clean_text = raw_text.replace('\n', '')
test.write(clean_text)

# closing the pdf file object
pdfFileObj.close()

# TODO Create json object with url, title, text, id for cloudsearch