import scholarly
from newspaper import Article
from requests_html import HTMLSession
from bs4 import BeautifulSoup
import re
from debatebot.exceptions import create_error


def extract_title(url):
    # TODO Finish this
    example_1 = "Support vector machines for 3D object recognition"
    example_2 = "Perception of physical stability and center of mass of 3-D objects"
    title = url
    return title


def get_papers(url):
    """
    Search by extracted title from uploaded pdf
    grab citation count
    DL html of paper
    grab related articles
    dl each related article and save the citation count
    Upload all to graph with a RELATED_TO relationship
    Query graph for top 3 cited articles related to users uploaded paper

    :return:
    """
    title = extract_title(url)
    related_url = ""
    host = "https://scholar.google.com/"

    # Search for user's paper on google scholar. Get related articles url
    # TODO have to make sure scholarly fork is being used. Might have to do the git clone and pip install -e . with dock
    search_query, soup = scholarly.search_pubs_query(title)

    # TODO Check if there is a pdf on website
    # Get html and download the user's paper
    uploaded_paper = next(search_query)
    paper_obj = {}
    art = Article(uploaded_paper.bib['url'])
    try:
        # Try and download. If we can't, we can still get the related so continue
        art.download()
        art.parse()
        paper_obj['text'] = art.text
        paper_obj['url'] = art.url
        paper_obj['title'] = art.title
        paper_obj['publish_date'] = art.publish_date
        paper_obj['citation_count'] = uploaded_paper.citedby
    except:
        return None, None, create_error("failed to download user paper")

    a_tags = soup.findAll("a", href=True)
    for tag in a_tags:
        if tag.contents and tag.contents[0] == "Related articles":
            link = tag['href']
            related_url = host + link
            break

    # fetch related_articles url
    # TODO Check if there is a pdf link so we don't get just the abstract
    link_list = []
    if related_url:
        session = HTMLSession()
        r = session.get(related_url)
        soup = BeautifulSoup(r.content, 'html.parser')

        # Find cited numbers for each paper. List is in same order as the urls of related papers below
        # TODO Extract the number only!
        def get_cited_number(text):
            return text and re.compile("Cited by").search(text)

        total = soup.find_all(string=get_cited_number)

        # Find urls of related papers and download and extract
        count = 0
        rel_links = soup.findAll("h3", class_="gs_rt")
        for tag in rel_links:
            if tag.a:
                if tag.a.get('href'):
                    temp_obj = {}
                    art = Article(tag.a.get('href'))
                    try:
                        art.download()
                        art.parse()
                        temp_obj['text'] = art.text
                        temp_obj['url'] = art.url
                        temp_obj['title'] = art.title
                        temp_obj['publish_date'] = art.publish_date
                        temp_obj['citation_count'] = total[count]
                        link_list.append(temp_obj)
                    except:
                        continue
                    count += 1
    else:
        return None, None, {"error": "no related articles link found."}

    return paper_obj, link_list, None
