from neo4j import GraphDatabase
from services.kg.search import get_papers
import os

class ConnectNeo4j(object):

    def __init__(self, ):
        # For debug neo4j local
        host = os.environ.get('NEO4J')
        self.driver = GraphDatabase.driver("bolt://"+host, auth=("neo4j", "fail"))

    def add_user_papers(self, tx, queries, paper):
        query = "MERGE (p:Paper {title: $title, text: $text, citations: $citation})"
        q = query + "\n" + queries
        tx.run(q, title=paper.get('title'), text=paper.get('text'), citation=paper.get('citation_count'))

    def create_merge_statements(self, rel_list):
        queries = []
        for i in rel_list:
            q = '''MERGE (p)-[:RELATED_TO]->(:Paper {title: "%s", text: "%s", citations: "%s"})''' % (
            i.get('title', "n/a "),
            i.get('text', "n/a "),
            i.get('citation_count', "n/a"))
            queries.append(q)
        return '\n'.join(queries)

    def get_top_papers(self, tx, title):
        papers = []
        records = tx.run("MATCH (p:Paper)-[:RELATED_TO]->(a) WHERE p.title = $title RETURN DISTINCT a"
                             , title=title)
        for record in records:
            if record["a"]["text"]:
                papers.append(record["a"]["text"])

        return papers


d = ConnectNeo4j()
print(d.driver)


# TEST Code To Upload outside of docker

# user_paper, rel_papers, error = get_papers("Support vector machines for 3D object recognition")
# neo = ConnectNeo4j()
# with neo.driver.session() as session:
#     q = neo.create_merge_statements(rel_papers)
#     try:
#         session.write_transaction(neo.add_user_papers, q, user_paper)
#     except Exception as e:
#         print("ERROR WRITING")
        # return Response(create_error("failed to write to kg"), status=400)

# example = [{"text": "nicole", "title": "relativity", "citation_count": 10},
#            {"text": "what", "title": "derp title", "citation_count": 12}]
#
# paper = {"text": "nicole is a dragon", "title": "original title", "citation": 5}
