
from unittest import TestCase
from services.kg.kg import ConnectNeo4j
from summarizer.summary import summary


class TestKG(TestCase):

    # def __init__(self, *args, **kwargs):
    #     super().__init__()

    def test_read_query(self):
        neo = ConnectNeo4j()
        title = "Perception of physical stability and center of mass of 3-D objects"
        with neo.driver.session() as session:
            papers = session.read_transaction(neo.get_top_papers, title)
            # self.assertEqual(len(papers), 8)
            # all_text = '\n\n\n########## '.join(papers)
            data = summary(' \n'.join(papers[:5]))
            with open('abc', 'w') as f:
                f.write(data)

    def test_write_query(self):
        neo = ConnectNeo4j()
