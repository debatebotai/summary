#!/bin/bash
python manage.py migrate    # Apply database migrations

# Prepare log files and start outputting logs to stdout
touch /code/logs/gunicorn.log
touch /code/logs/access.log
tail -n 0 -f /code/logs/*.log & # Monitors changes in file and outputs

echo Starting nginx
# Start Gunicorn processes
echo Starting Gunicorn.
# debatebot.sock and debatebot.wsgi or debatebot-back.debatebot.wsgi
exec gunicorn debatebot.wsgi:application \
    --name Summary \
    --bind unix:/code/debatebot/Summary.sock \
    --workers 2 \
    --log-level=info \
    --log-file=/code/logs/gunicorn.log \
    --access-logfile=/code/logs/access.log \
    --timeout 10000 &

exec service nginx start