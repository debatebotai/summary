# DebateBot Question/Answer API



#### Setup Locally

First activate your environment and do pip install reqs/requirements.txt

#### Create the word2vec binary file

    python text-proc/word-embeddings.py

You only have to run this once.

#### Create the Gensim Article Shards for Article Search
    python text-proc/article-shards.py

You only have to run this once.

This will create the article doc2vec for the article search. This create multiple shards for quick search

#### Run the startup-script to load the word2vec into memory. Leave the process running in another terminal.
    python startup-scripts/word2vec-start.py

#### Finally, run the server
    python manage.py runserver

Now go to 127.0.0.1:8000 and enter your question.

If you enter an article, it will only search the article, otherwise,
it will find an article in our database on Dynaomo (currently only 200 available)


### Setup AWS

- Launch the aws instance with the correct dbot-django image in Asia Pacific or East

    a. Password for vncserver is dbot54
    
- VNCserver automatically starts up with instance so you can connect using the domain name
for the instance.
- Env is setup with conda environment dbot for aws.
- Project Directory is located in dbot folder on Desktop. Model files for training can be put in rnet
folder to separate from django project.

#### Activate env
    source activate dbot


#### Runs server From Local Dir.
```
python manage.py runserver
```


### Upload Documents to DynamoDB


