from rest_framework import viewsets
from rest_framework.renderers import JSONRenderer

from authorization.models import User
from authorization.serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    renderer_classes = [JSONRenderer]

    #
    # def create():
    #
    #     # TODO SET permissions
    #     pass
