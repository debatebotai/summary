from django.conf.urls import url, include
from rest_framework import routers
from authorization.views import UserViewSet
from rest_framework.urlpatterns import path


router = routers.DefaultRouter()
router.register(r'', UserViewSet)

urlpatterns = [
    path('users/', include(router.urls)),
    path('summary/', include('summarizer.urls')),
]